FROM maven:3.6.3 AS build
WORKDIR /opt/       
COPY . /opt/   
RUN mvn clean install -Dmaven.test.skip=true




FROM adoptopenjdk/openjdk11:alpine-jre   
ARG JAR_FILE=studentsystem-0.0.1-SNAPSHOT.jar
WORKDIR /opt/
COPY --from=build /opt/target/${JAR_FILE} /opt/      

ENTRYPOINT [ "java", "-jar", "/opt/${JAR_FILE}" ]
